﻿using Oracle.ManagedDataAccess.Client;
using ProdutoWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProdutoWeb.DAO
{
    public class TipoContatoDAO
    {

        public IList<TipoContato> ListarTodos()
        {
            DAO.DataBaseContext ctx = new DAO.DataBaseContext();
            IList<TipoContato> ListaTipoContato = ctx.TipoContato.ToList<TipoContato>();

            return ListaTipoContato;
        }



        public TipoContato BuscaPorId(int Id)
        {

            DAO.DataBaseContext ctx = new DAO.DataBaseContext();
            return ctx.TipoContato.Find(Id);
            
        }


        public void Inserir(TipoContato Tipo)
        {
            DAO.DataBaseContext ctx = new DAO.DataBaseContext();
            ctx.TipoContato.Add(Tipo);
            ctx.SaveChanges();
        }


        public void Excluir(int Id)
        {
            DataBaseContext DBContext = new DataBaseContext();
            TipoContato tipoContato = BuscaPorId(Id);
            DBContext.Entry(tipoContato).State = System.Data.Entity.EntityState.Deleted;
            DBContext.SaveChanges();

        }


        public void Alterar(TipoContato Tipo)
        {
            DataBaseContext DBContext = new DataBaseContext();
            DBContext.Entry(Tipo).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();
        }

    }
}
 
 
 