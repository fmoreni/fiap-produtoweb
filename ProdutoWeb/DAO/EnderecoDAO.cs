﻿using ProdutoWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdutoWeb.DAO
{
    public class EnderecoDAO
    {

        public Endereco ConsultarEndereco(String CEP)
        {
            Endereco End = new Endereco();

            if ( CEP.Equals("10000000"))
            {
                End.IdTipoLogradouro = 1;
                End.NomeEndereco = "Paulista";
                End.BairroEndereco = "Bairro Paulista";
                End.CidadeEndereco = "São Paulo - Capital";
                End.SiglaEstado = "SP";

            } else if (CEP.Equals("20000000"))
            {

                End.IdTipoLogradouro = 1;
                End.NomeEndereco = "Delfim Moreira";
                End.BairroEndereco = "Leblon";
                End.CidadeEndereco = "Rio de Janeiro";
                End.SiglaEstado = "RJ";

            } else if (CEP.Equals("30000000"))
            {

                End.IdTipoLogradouro = 2;
                End.NomeEndereco = "Costa e Silva";
                End.BairroEndereco = "Centro";
                End.CidadeEndereco = "Curitiba";
                End.SiglaEstado = "PR";

            } else
            {

                End.IdTipoLogradouro = 3;
                End.NomeEndereco = "João Carlos Arruda";
                End.BairroEndereco = "Centro";
                End.CidadeEndereco = "Floripa";
                End.SiglaEstado = "SC";

            }

            // TO DO
            return End;
        }


        public void GravarEndereco(Endereco End)
        {
            // TO DO
            Console.Write(End);
        }


        public List<EstadoUF> ListarEstados()
        {
            List<EstadoUF> Lista = new List<EstadoUF>();

            Lista.Add(new EstadoUF { NomeEstado = "São Paulo", SiglaEstado = "SP" } );
            Lista.Add(new EstadoUF { NomeEstado = "Rio de Janeiro", SiglaEstado = "RJ" });
            Lista.Add(new EstadoUF { NomeEstado = "Paraná", SiglaEstado = "PR" });
            Lista.Add(new EstadoUF { NomeEstado = "Santa Catarina", SiglaEstado = "SC" });

            return Lista;
        }


        public List<TipoLogradouro> ListarTipoLogradouros()
        {
            List<TipoLogradouro> Lista = new List<TipoLogradouro>();

            Lista.Add(new TipoLogradouro { NomeTipoLogradouro = "Avenida", IdTipoLogradouro = 1 });
            Lista.Add(new TipoLogradouro { NomeTipoLogradouro = "Rua", IdTipoLogradouro = 2 });
            Lista.Add(new TipoLogradouro { NomeTipoLogradouro = "Praça", IdTipoLogradouro = 3 });
            Lista.Add(new TipoLogradouro { NomeTipoLogradouro = "Rodovia", IdTipoLogradouro = 4 });

            return Lista;
        }

      

    }
}