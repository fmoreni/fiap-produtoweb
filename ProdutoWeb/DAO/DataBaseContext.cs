﻿using ProdutoWeb.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ProdutoWeb.DAO
{
    public class DataBaseContext: DbContext
    {

        public DataBaseContext() : base("name=OracleDbContext")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DataBaseContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(ConfigurationManager.AppSettings["DatabaseSchema"].ToString());
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<TipoContato> TipoContato { get; set; }
        public DbSet<Contato> Contato { get; set; }
        public DbSet<Produto> Produto { get; set; }
        public DbSet<Imagem> Imagem { get; set; }
        public DbSet<Log> Log { get; set; }

    }
}