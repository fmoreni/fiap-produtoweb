﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProdutoWeb.Models;

namespace ProdutoWeb.DAO
{
    public class ProdutoDAO
    {

        public IList<Produto> ListarTodos()
        {
            return new DataBaseContext().Produto.ToList<Produto>();
        }

        public Produto ConsultarPorId(int Id)
        {
            return new DataBaseContext().Produto.Find(Id);
        }

        public void Inserir(Produto Prod)
        {
            Prod.Ativo = true;
            DataBaseContext DBContext = new DataBaseContext();
            DBContext.Produto.Add(Prod);
            DBContext.SaveChanges();
        }

        public void Editar(Produto Prod)
        {
            DataBaseContext DBContext = new DataBaseContext();
            DBContext.Entry(Prod).State = System.Data.Entity.EntityState.Modified;
            DBContext.SaveChanges();
        }

        public void Excluir(int Id)
        {
            DataBaseContext DBContext = new DataBaseContext();
            Produto Produto = ConsultarPorId(Id);
            DBContext.Entry(Produto).State = System.Data.Entity.EntityState.Deleted;
            DBContext.SaveChanges();
        }


        public Produto ConsultarPorNome(String Nome)
        {
            Produto Produto = new Produto();

            try { 
                DataBaseContext DBContext = new DataBaseContext();
                Produto = DBContext.Produto.Where(p => p.NomeProduto == Nome).SingleOrDefault<Produto>();
            }
            catch (Exception ex)
            {
                Produto = new Produto();
            }

            return Produto;
        }


        public List<Produto> ConsultarParteNome(String ParteNome)
        {
            var ListaRetorno = new DataBaseContext()
                    .Produto
                    .Where( 
                            p => p.NomeProduto.Contains(ParteNome) &&  
                            p.Ativo == true
                           )
                    .OrderBy( p => p.NomeProduto)
                    .ToList<Produto>();

            return ListaRetorno;
        }

        
        public List<Produto> ConsultarParteNomeDinamico(String ParteNome, String ParteDescricao)
        {
            var ListaRetorno = new DataBaseContext()
                    .Produto
                    .Where
                    (
                        p => 
                        (string.IsNullOrEmpty(ParteNome) || p.NomeProduto.Contains(ParteNome) ) &&
                        (string.IsNullOrEmpty(ParteDescricao) || p.DescricaoProduto.Contains(ParteDescricao) ) &&
                        ( p.Ativo == true )

                    ).ToList<Produto>();


            return ListaRetorno;
        }

    }
}