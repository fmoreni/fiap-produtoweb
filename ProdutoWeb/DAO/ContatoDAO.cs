﻿
using ProdutoWeb.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProdutoWeb.DAO
{
    public class ContatoDAO
    {

        public void Inserir(Contato Cont)
        {
            DataBaseContext DBContext = new DataBaseContext();
            DBContext.Contato.Add(Cont);
            DBContext.SaveChanges();

        }

        public List<Contato> Filtrar(string email, string nome)
        {
            List<Contato> ListaRetorno = new List<Contato>();

            using (DataBaseContext DBContext = new DataBaseContext())
            {
                ListaRetorno =
                    DBContext.
                        Contato.
                            Where
                                (
                                    c =>
                                        (string.IsNullOrEmpty(email) || c.Email.Contains(email)) &&
                                        (string.IsNullOrEmpty(nome) || c.Nome.Contains(nome))
                                )
                                .OrderBy(c => c.Nome)
                                .ToList<Contato>();

            }

            return ListaRetorno;
        }

    }
}