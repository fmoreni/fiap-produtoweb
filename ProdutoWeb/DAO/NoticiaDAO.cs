﻿using ProdutoWeb.Models;
using System.Collections.Generic;

namespace ProdutoWeb.DAO
{
    public class NoticiaDAO
    {

        public IList<Produto> ListarTodosProdutos()
        {
            IList<Produto> Lista = new List<Produto>();
            Lista.Add(new Produto { IdProduto = 1, NomeProduto = "Prod 1" });
            Lista.Add(new Produto { IdProduto = 2, NomeProduto = "Prod 2" });
            Lista.Add(new Produto { IdProduto = 3, NomeProduto = "Prod 3" });
            return Lista;

        }


        public IList<Noticia> ListarNoticiasPorProduto(int IdProd)
        {
            IList<Noticia> Lista = new List<Noticia>();

            if ( IdProd == 1 )
            {
                Lista.Add(new Noticia { IdNoticia = 1, TituloNoticia = "Noticia 1 - Prod1", DataNoticia = "01/01/2000" });
                Lista.Add(new Noticia { IdNoticia = 2, TituloNoticia = "Noticia 2 - Prod1", DataNoticia = "01/02/2000" });
                Lista.Add(new Noticia { IdNoticia = 3, TituloNoticia = "Noticia 3 - Prod1", DataNoticia = "01/03/2000" });
                //Lista.Add(new Noticia { IdNoticia = 4, TituloNoticia = "Noticia 4 - Prod1", DataNoticia = "01/04/2000" });
            }

            if (IdProd == 2)
            {
                Lista.Add(new Noticia { IdNoticia = 5, TituloNoticia = "Noticia 1 - Prod2", DataNoticia = "01/01/2000" });
                Lista.Add(new Noticia { IdNoticia = 6, TituloNoticia = "Noticia 2 - Prod2", DataNoticia = "01/02/2000" });
                //Lista.Add(new Noticia { IdNoticia = 7, TituloNoticia = "Noticia 3 - Prod2", DataNoticia = "01/03/2000" });
                //Lista.Add(new Noticia { IdNoticia = 8, TituloNoticia = "Noticia 4 - Prod2", DataNoticia = "01/04/2000" });
            }

            if (IdProd == 3)
            {
                Lista.Add(new Noticia { IdNoticia = 9, TituloNoticia = "Noticia 1 - Prod3", DataNoticia = "01/01/2000" });
                Lista.Add(new Noticia { IdNoticia = 10, TituloNoticia = "Noticia 2 - Prod3", DataNoticia = "01/02/2000" });
                Lista.Add(new Noticia { IdNoticia = 11, TituloNoticia = "Noticia 3 - Prod3", DataNoticia = "01/03/2000" });
                Lista.Add(new Noticia { IdNoticia = 12, TituloNoticia = "Noticia 4 - Prod3", DataNoticia = "01/04/2000" });
            }


            return Lista;

        }



    }
}