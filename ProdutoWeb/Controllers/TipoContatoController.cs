﻿using ProdutoWeb.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using ProdutoWeb.DAO;

namespace ProdutoWeb.Controllers
{
    public class TipoContatoController : Controller
    {
        // GET: TipoContato
        public ActionResult Index()
        {

            ViewBag.ListaTipoContato = new TipoContatoDAO().ListarTodos();
            return View();

        }
    }
}