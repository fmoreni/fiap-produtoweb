﻿using ProdutoWeb.DAO;
using ProdutoWeb.Filter;
using ProdutoWeb.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{
    public class TesteController : Controller
    {
        // GET: Teste
        [HttpGet]
        [LogFilter]
        public ActionResult Index()
        {

            // Listando todos os Produtos
            using (DataBaseContext ctx = new DataBaseContext())
            {
                IList<Produto> ListaProdutos = 
                    ctx.Produto.
                        Include("Imagens").
                            ToList<Produto>();
            }



                /**
                 * Recuperando uma Imagem e o seu Produto
                 * 
                 * */
                /*
                int IdImgSelecionada = 4;
                using (DataBaseContext ctx = new DataBaseContext())
                {
                    Imagem ImagemConsulta = 
                        ctx.Imagem.
                            Include("Produto").
                                Single(i => i.IdImagem == IdImgSelecionada);
                }
                */


                /*
                int IdProdSelecionado = 21;
                using (DataBaseContext ctx = new DataBaseContext())
                {
                    Produto ProdutoConsultado = 
                        ctx.Produto.
                            Include("Imagens").
                                Single(p => p.IdProduto == IdProdSelecionado);
                }
                */

                /*
                 * RECUPERANDO UM PRODUTO E AS IMAGENS.
                 * 
                 * 


                */

                /* 
                 * 
                 * ADICIONANDO O PRODUTO E A IMAGEM
                 * 
                 * 
                using ( DataBaseContext ctx = new DataBaseContext() )
                {
                    IList<Imagem> ListaImagens = new List<Imagem>();

                    Imagem imagem1 = new Models.Imagem();
                    imagem1.Rotulo = "Imagem do Produto 4.1";
                    imagem1.Caminho = "/img/prod4-1.jpg";
                    ListaImagens.Add(imagem1);

                    Imagem imagem2 = new Models.Imagem();
                    imagem2.Rotulo = "Imagem do Produto 4.1";
                    imagem2.Caminho = "/img/prod4-2.jpg";
                    ListaImagens.Add(imagem2);


                    Produto prod = new Models.Produto();
                    prod.NomeProduto = "Produto 4";
                    prod.DescricaoProduto = "Desc Prod 4";
                    prod.PrecoProduto = 44.44;
                    prod.CaracteristicasProduto = "Carac Prod4";
                    prod.ImagemPadrao = "img/image4.jpg";
                    prod.Ativo = true;
                    prod.Imagens = ListaImagens;

                    ctx.Produto.Add(prod);
                    ctx.SaveChanges();

                    System.Console.Write("Teste");

                }
                /*


                /*
                TipoContatoDAO dao = new TipoContatoDAO();
                TipoContato tipo = dao.BuscaPorId(3);
                tipo.NomeTipoContato = "Colaboradores";
                dao.Alterar(tipo);
                */

                return View();

        }
    }
}