﻿using ProdutoWeb.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{
    public class ProdutoAPIController : Controller
    {
        [HttpGet]
        public JsonResult Index()
        {

            ProdutoDAO ProdDAO = new ProdutoDAO();
            IList<Models.Produto> lista = ProdDAO.ListarTodos();

            return Json( lista, JsonRequestBehavior.AllowGet );

        }
    }
}