﻿using ProdutoWeb.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers.JSON
{
    public class TipoContatoAPIController : Controller
    {
        // GET: TipoContatoAPI
        public JsonResult Index()
        {
            return Json( new TipoContatoDAO().ListarTodos(), JsonRequestBehavior.AllowGet  );
        }
    }
}