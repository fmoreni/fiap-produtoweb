﻿using ProdutoWeb.DAO;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{
    public class EnderecoController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.ListaEstados = new EnderecoDAO().ListarEstados();
            return View();
        }

        [HttpPost]
        public JsonResult Gravar(Models.Endereco End)
        {
            return Json(End);
        }

        [HttpGet]
        public JsonResult ListarTipoLogradouro()
        {
            return Json(new EnderecoDAO().ListarTipoLogradouros(),JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ConsultarEndereco(string CEP)
        {
            return Json(new EnderecoDAO().ConsultarEndereco(CEP), JsonRequestBehavior.AllowGet);
        }

    }
}