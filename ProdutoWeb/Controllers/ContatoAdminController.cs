﻿using ProdutoWeb.DAO;
using ProdutoWeb.Models;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{
    public class ContatoAdminController : Controller
    {
        [HttpGet]
        public ActionResult Index()  
        {
            return View();
        }


        [HttpPost]
        public ActionResult Index(Contato ContatoDigitado)
        {
           
            ViewBag.ListaContatoPesquisados =
                new ContatoDAO()
                    .Filtrar(
                        ContatoDigitado.Email,
                        ContatoDigitado.Nome);

            return View();
        }
    }
}