﻿using ProdutoWeb.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{
    public class NoticiasController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public JsonResult ListarTodosProdutos()
        {
            return Json(new NoticiaDAO().ListarTodosProdutos(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult ListarNoticiasPorProduto(int Id)
        {
            return Json(new NoticiaDAO().ListarNoticiasPorProduto(Id), JsonRequestBehavior.AllowGet);
        }

    }
}