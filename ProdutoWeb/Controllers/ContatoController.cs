﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProdutoWeb.Models;
using ProdutoWeb.DAO;

namespace ProdutoWeb.Controllers
{
    public class ContatoController : Controller
    {

        [HttpGet]
        public ActionResult Index(Models.Contato Contato)
        {
            IList<TipoContato> ListaTipoContato = new TipoContatoDAO().ListarTodos();
            ViewBag.ListaTipoContato = ListaTipoContato;
            ModelState.Clear();
            return View(Contato);
        }

        [HttpPost]
        public ActionResult Gravar(Models.Contato Contato)
        {

            if (ModelState.IsValid)
            {
                new ContatoDAO().Inserir(Contato);
                Contato = new Contato();
                ModelState.Clear();
                TempData["Mensagem"] = "Contato recebido e armazenado em nosso banco de dados. Obrigado!";

            } else
            {
                // ENVIAR O USUÁRIO PARA TELA DE ERRO.
                TempData["Mensagem"] = "Não foi possível cadastrar as informações!";

            }

            IList<TipoContato> ListaTipoContato = new TipoContatoDAO().ListarTodos();
            ViewBag.ListaTipoContato = ListaTipoContato;
            return View("Index", Contato);

        }

    }
}