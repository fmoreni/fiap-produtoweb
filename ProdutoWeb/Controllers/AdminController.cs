﻿using ProdutoWeb.DAO;
using ProdutoWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProdutoWeb.Controllers
{
    public class AdminController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            HttpCookie cookie = Request.Cookies["UsuarioProjetoWeb"];
            if (cookie != null)
            {
                String Email = cookie["Email"].ToString();
                String Senha = cookie["Senha"].ToString();
                Usuario UsuarioLogin = new Usuario(Email,Senha);

                Usuario UsuarioLogado = new UsuarioDAO().Login(UsuarioLogin.EmailUsuario, UsuarioLogin.SenhaUsuario);

                if (UsuarioLogado != null)
                {
                    Session["UsuarioLogado"] = UsuarioLogado;
                    cookie.Expires = DateTime.Now.AddDays(360);
                    Response.AppendCookie(cookie);

                    return View("Login");
                } else
                {
                    return View();
                }

            } else
            {
                return View();
            }

        }

        [HttpPost]
        public ActionResult Login(Usuario UsuarioLogin)
        {

            UsuarioDAO UserDAO = new UsuarioDAO();
            Usuario UsuarioLogado = UserDAO.Login(UsuarioLogin.EmailUsuario, UsuarioLogin.SenhaUsuario);

            if (UsuarioLogado != null )
            {
                // Usuario Encontrado e Login efetuado com sucesso.

                // Adicionando o usuário na Sessão
                Session["UsuarioLogado"] = UsuarioLogado;

                // Criando um Cookie
                HttpCookie cookie = new HttpCookie("UsuarioProjetoWeb");
                cookie.Values.Add("Email", UsuarioLogin.EmailUsuario);
                cookie.Values.Add("Senha", UsuarioLogin.SenhaUsuario);
                cookie.Expires = DateTime.Now.AddDays(360);
                Response.AppendCookie(cookie);


                // Exibindo a Tela com Login
                return View();
            }

            // Falha no Login
            else
            {
                TempData["Mensagem"] = "Usuário ou Senha inválida.";
                return View("Index", UsuarioLogin);
            }
        }



        public ActionResult Logoff()
        {
            Session.Clear();

            HttpCookie cookie = Request.Cookies["UsuarioProjetoWeb"];
            cookie.Expires = DateTime.Now.AddDays(-1);
            Response.AppendCookie(cookie);

            return View("Index");
        }

    }
}