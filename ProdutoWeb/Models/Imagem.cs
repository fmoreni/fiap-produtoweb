﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Script.Serialization;


namespace ProdutoWeb.Models
{
    [Table("NETIMAGEMPRODUTO")]
    public class Imagem
    {

        [Key]
        [Column("IDIMAGEM")]
        public int IdImagem { get; set; }

        [Column("ROTULO")]
        public String Rotulo { get; set; }

        [Column("CAMINHO")]
        public String Caminho { get; set; }

        [Column("IDPRODUTO")]
        public int IdProduto { get; set; }

        [ScriptIgnore]
        public Produto Produto { get; set; }

    }
}