﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdutoWeb.Models
{
    public class Noticia
    {

        public int IdNoticia { get; set; }

        public String TituloNoticia { get; set; }

        public String DataNoticia { get; set; }


    }
}