﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProdutoWeb.Models
{

    [Table("NETCONTATO")]
    public class Contato
    {
        
        [Key]
        [Column("IDCONTATO")]
        public int IdContato { get; set; }


        [Column("NOMECONTATO")]
        [Display(Name = "Nome", Description = "Digite o nome")]
        [Required(ErrorMessage = "Nome obrigatório")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "O campo Nome deve possuir no mínimo 3 e no máximo 50 caracteres")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$", ErrorMessage = "Digite o nome sem números e caracteres especiais.")]
        public string Nome { get; set; }


        [Column("EMAIL")]
        [Required(ErrorMessage = "e-Mail obrigatório")]
        [EmailAddress]
        public string Email { get; set; }


        [Column("MENSAGEM")]
        [Display(Name = "Mensagem", Description = "Digite o texto da mensagem")]
        [Required(ErrorMessage = "Mensagem Obrigatória")]
        [StringLength(2048, MinimumLength = 3, ErrorMessage = "O campo Nome deve possuir no mínimo 3 e no máximo 2048 caracteres")]
        public string Mensagem { get; set; }


        [Column("IDTIPOCONTATO")]
        public int IdTipoContato { get; set; }


        public TipoContato TipoContato { get; set; }


    }
}