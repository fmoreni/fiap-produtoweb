﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdutoWeb.Models
{
    public class Endereco
    {

        public int IdEndereco { get; set; }

        public String CepEndereco { get; set; }

        public String NomeEndereco { get; set; }

        public int NumeroEndereco { get; set; }

        public String ComplementoEndereco { get; set; }

        public String BairroEndereco { get; set; }

        public String CidadeEndereco { get; set; }

        public String SiglaEstado { get; set; }

        public int IdTipoLogradouro { get; set; }

    }
}