﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace ProdutoWeb.Models
{

    [Table("NETTIPOCONTATO")]
    public class TipoContato
    {

        [Key]
        [Column("IDTIPOCONTATO")]
        public int IdTipoContato { get; set; }

        [Column("NOMETIPOCONTATO")]
        public string NomeTipoContato { get; set; }

    }

}