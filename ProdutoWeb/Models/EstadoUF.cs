﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdutoWeb.Models
{
    public class EstadoUF
    {
        public String SiglaEstado { get; set; }
        public String NomeEstado { get; set; }
    }
}