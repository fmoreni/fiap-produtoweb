﻿using ProdutoWeb.Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProdutoWeb.Models
{

    [Table("NETPRODUTO")]
    public class Produto
    {

        public Produto()
        {
            Imagens = new Collection<Imagem>();
        }


        [Key]
        [Column("IDPRODUTO")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdProduto { get; set; }

        [Column("NOMEPRODUTO")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "O campo Nome deve possuir no mínimo 3 e no máximo 50 caracteres")]
        [Display(Name = "Nome", ResourceType = typeof(ProdutoResource), Description = "Digite o nome")]
        [Required(ErrorMessageResourceName = "NomeProdutoRequerido", ErrorMessageResourceType = typeof(ProdutoResource))]
        public String NomeProduto { get; set; }

        [Column("DESCRICAOPRODUTO")]
        [Display(Name = "Descrição", Description = "Digite a descrição")]
        [Required(ErrorMessage = "Descrição é obrigatório")]
        [StringLength(200, MinimumLength = 3, ErrorMessage = "O campo Descrição deve possuir no mínimo 3 e no máximo 200 caracteres")]
        public String DescricaoProduto { get; set; }

        [Column("PRECOPRODUTO")]
        [Display(Name = "Preço", Description = "Digite o Preço")]
        [Required(ErrorMessage = "Preço é obrigatório")]
        [Range(0, Double.PositiveInfinity)]
        public double PrecoProduto { get; set; }


        [Column("CARACTERISTICASPRODUTO")]
        [Display(Name = "Características", Description = "Digite a(s) característica(s)")]
        [Required(ErrorMessage = "Características é obrigatório")]
        [StringLength(2000, MinimumLength = 3, ErrorMessage = "O campo Características deve possuir no mínimo 3 e no máximo 2000 caracteres")]
        public String CaracteristicasProduto { get; set; }

        [Column("IMAGEMPADRAO")]
        [Display(Name = "Imagem", Description = "Digite a url da imagem")]
        [StringLength(2000, MinimumLength = 3, ErrorMessage = "O campo Imagem deve possuir no mínimo 3 e no máximo 200 caracteres")]
        public String ImagemPadrao { get; set; }


        [Column("ATIVO")]
        public bool Ativo { get; set; }


        public virtual ICollection<Imagem> Imagens { get; set; }

    }
}