﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdutoWeb.Models
{
    public class Usuario
    {
        public Usuario()
        {

        }

        public Usuario(String Email, String Senha)
        {
            this.EmailUsuario = Email;
            this.SenhaUsuario = Senha;
        }

        public int IdUsuario { get; set; }
        public String NomeUsuario { get; set; }
        public String EmailUsuario { get; set; }
        public String SenhaUsuario { get; set; }
        
    }
}