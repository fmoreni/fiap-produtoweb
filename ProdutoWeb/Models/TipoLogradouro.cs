﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProdutoWeb.Models
{
    public class TipoLogradouro
    {

        public int IdTipoLogradouro { get; set; }

        public String NomeTipoLogradouro { get; set; }

    }
}