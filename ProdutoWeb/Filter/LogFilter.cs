﻿using System;
using System.Web.Mvc;
using ProdutoWeb.Models;
using ProdutoWeb.DAO;

namespace ProdutoWeb.Filter
{
    public class LogFilter: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Log log = new Log();
            log.DataLog = DateTime.Now;
            log.Caminho = filterContext.HttpContext.Request.Url.ToString();
            new LogDAO().Inserir(log);
        }
    }
}

